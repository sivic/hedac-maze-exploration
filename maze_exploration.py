# -*- coding: utf-8 -*-
"""
This is the main class for the maze exploration algorithm presented in

"Fast algorithm for centralized multi-agent maze exploration." by
Crnković, Bojan, Stefan Ivić, and Mila Zovko
arXiv preprint arXiv:2310.02121 (2023)
https://arxiv.org/abs/2310.02121
"""

import numpy as np
import os
import shutil
import matplotlib.pyplot as plt
import time
import subprocess
import scipy.optimize


class MazeExploration:
    """
    Main class for the maze exploration.
    """

    def __init__(self, results_dir: str, maze: str):
        """
        Initialize the maze exploration

        Parameters
        ----------
        results_dir : str
            A sting containing the path of the directory where the results are saved.
        maze : str
            A string defining a base file name for the maze walls (maze_h.txt and maze_v.txt are expected to exist).
        """

        self.results_dir = results_dir
        """A string representing the path to the directory where the results will be saved."""

        self.maze = maze
        """A string defining a base file name for the maze walls (maze_h.txt and maze_v.txt are expected to exist)."""

        self.visited = None
        """Matrix that counts number of (agent) visits for each cell."""

        self.occupied = None
        """A bool matrix indicating whether the cell is currently occupied by the agent."""

        self.visible = False
        """Visibility of cells. Initially can be set to True or False if entire maze is visible or not, respectively.
        After initialization, this variable is converted to a matrix that provides visibility status of each cell."""

        self.first_visit = False
        """A matrix containing time step at which the corresponding cells are found (first visited). For yet unvisited
        cells the value in the matrix is -1."""

        self.allow_collision = False
        """A parameter that regulates whether multiple agents can occupy the same cell at the same time.
        Defaults to False."""

        self.max_it = 100_000
        """Maximum number of time steps."""

        self.SORomega = 1.25
        """Relaxation parameter of SOR solver"""

        self.eps = 1e-1
        """Max Relative error of SOR solver"""

        self.maxSOR_it = 1000
        """Maximum number of steps in SOR solver"""

        self.na = 2
        """Number of agent employed to explore the maze."""

        self.agents = None
        """The [na, nt, 2] shaped array of all agents locations. Initially it can be used as [na, 2] shaped array for 
        setting agent initial locations"""

        self.target = None
        """List of row and column positions of the target cell."""

        self.verbose = True
        """Controls whether the maze exploration prints any information on the screen and makes summary figures."""

        self.plot = False
        """Parameter determing whether to plot the maze exploration at each time step or not. Plotting is time 
        consuming compared to solving the agents' motion."""

        self.plotstep=1
        self.solver = 'DGESV'
        """The solver used for solving the linear system for the potential at each time step. The defulat sover is 
        'DGESV' (via numpy.linalg.solve), while other option is 'SOR' iterative solver."""

        self.A = None
        """The system matrix for finite difference approximation of the heat equation. The matrix represents linear 
        equations for all cells of the maze, but prior to solving a sub-matrix is extracted for only visible cells."""

        self.neumann_bc_order = 2
        """Order of Neumann boundary condition approx. v_i=v_i+1 for order 1 and using ghost cell v_i-1=v_i+1"""

        self.ScaleB= 1.
        """Scaleing factor of the right hand side to reduce roundoff errors near zero"""
        
        self.refreshSORomegaStep=20
        """SOR relaxation parameter refresh step"""
        
        self.refreshSOR=True
        """If TRUE SOR relaxation parameter is calcualted automaticaly"""

        self.history = dict()
        """A dictionary containing convergence results for the maze exploration."""

    def init(self):
        """Initialization of maze exploration called after all parameter have been set."""

        # Removing existing and creating results directory
        if os.path.exists(self.results_dir):
            shutil.rmtree(self.results_dir)
        os.makedirs(self.results_dir)

        # Set time index to zero
        self.it = 0

        # Initialize the maze
        self.load_ascii_maze(self.maze)

        # Counting cell visits
        self.visited = np.zeros([self.ny, self.nx], dtype=int)

        self.first_visit = np.full([self.ny, self.nx], -1)

        # Visibility of cells
        self.visible = np.logical_and(self.visible, np.ones([self.ny, self.nx], dtype=bool))

        # Occupancy of cells
        self.occupied = np.zeros([self.ny, self.nx], dtype=bool)

        # Potential matrix
        self.U = np.full([self.ny, self.nx], np.nan)

        # Initilize agents
        print(np.shape(self.agents))
        assert isinstance(self.agents, int) or isinstance(self.agents, np.ndarray) \
            or self.agents is None, 'agents parameter must be an integer or numpy array'
        if isinstance(self.agents, int):
            self.na = self.agents
            self.agents = -np.ones([self.na, self.max_it + 1, 2], dtype=int)
            self.agents[:, 0, 0] = np.random.randint(0, self.ny, self.na)
            self.agents[:, 0, 1] = np.random.randint(0, self.nx, self.na)
        elif isinstance(self.agents, np.ndarray):
            assert self.agents.ndim == 2 and self.agents.shape[1] == 2, \
                'agents parameter must two-dimensional array with two columns'
            self.na = int(self.agents.size / 2)
            a0 = np.copy(self.agents)
            self.agents = -np.ones([self.na, self.max_it + 1, 2], dtype=int)
            self.agents[:, 0, :] = a0
        # print(self.na)
        # input('?')

        assert self.solver in 'DGESV SOR'.split(), \
            f'Invalid solver is specified ({self.solver})'

        # Update convergence
        self.history['solver_time'] = np.full([0, self.na], np.nan)
        self.history['preparation_time'] = np.full([0, self.na], np.nan)
        self.history['step_time'] = np.array([])
        self.history['coverage'] = np.array([])
        self.history['sor_iterations'] = np.full([0, self.na], np.nan)

        # Plot maze layout
        if self.verbose:
            self.plot_maze(f'{self.results_dir}/maze.png')

        # SOR solver init list
        if self.solver == 'SOR':
            # list of red nodes
            self.zR = np.full([0],0, dtype=np.uint32)
            # list of black nodes
            self.zB = np.full([0],0, dtype=np.uint32)
            # diagonal
            self.scale = np.zeros(self.ny * self.nx)
            # list of all neigbours and if some are missing set to the last node of u
            self.neigbours = np.ones((self.ny * self.nx, 4), dtype=np.uint32, order='F')
            # set all to last node of u
            self.neigbours *= self.ny * self.nx
            # right hand side
            self.B = np.zeros(self.ny * self.nx)

            # Potential field 
            # one ekstra item is added because SOR runs faster if there is fixed number of neighbors
            self.u = np.zeros(self.ny * self.nx + 1)

            self.totalIterations = 0.

    def sor_add_new_cell(self, updated):
        """Updates the entire maze cell visibility matrix for given agent position. The cell visibility depends on the
        internal walls of the maze.

        Parameters
        ----------
        updated: list [list [int]]
            List of pairs [iy,ix] where iy is row index and ix cell index of a cell visited by the agent.
        """
        for p in updated:
            self.addSOR(p[0], p[1])
        self.zR.sort()
        self.zB.sort()

    def load_ascii_maze(self, filename):
        """Loads the maze from an ASCII file (sets the maze cells and walls). It is assumed that ASCII file contains
        outer and inner walls represented by '#' character, while passable corridors are represented by space character
        ' '. Only inner walls are processed while all outer walls are forced to ensure the maze is closed.

        Parameters
        ----------
        filename : str
            A file name (path) of ASCII file containing the maze topology.
        """

        with open(filename) as maze_file:
            lines = maze_file.readlines()

        walls_h = []
        for line in lines[0::2]:
            walls_h.append([c == '#' for c in line.strip()[1::2]])
        walls_h = np.array(walls_h, dtype=int)
        walls_h = walls_h[1:-1, :]

        walls_v = []
        for line in lines[1::2]:
            walls_v.append([c == '#' for c in line.strip()[0::2]])
        walls_v = np.array(walls_v, dtype=int)
        walls_v = walls_v[:, 1:-1]

        self.nx = walls_h.shape[1]
        self.ny = walls_v.shape[0]
        self.walls_h = np.append(np.append(np.ones([1, self.nx], dtype=bool), walls_h, axis=0),
                                 np.ones([1, self.nx], dtype=bool), axis=0)
        self.walls_v = np.append(np.append(np.ones([self.ny, 1], dtype=bool), walls_v, axis=1),
                                 np.ones([self.ny, 1], dtype=bool), axis=1)
        self.walls_h = self.walls_h[::-1, :]
        self.walls_v = self.walls_v[::-1, :]

    def save_ascii_maze(self, filename):
        """Saves ascii representation of the maze to given file name. The ASCII file contains all walls (outer and
        inner walls) represented by '#' character, while passable corridors are represented by space character
        ' '.

        Parameters
        ----------
        filename : str
            A file name (path) of ASCII file to which the maze topology is to be saved.
        """

        lines = []
        for iy in range(self.ny * 2 + 1):
            if iy % 2:  # odd, vertical
                walls = self.walls_v[(iy - 1) // 2, :]
                line = ' '.join(['#' if w else ' ' for w in walls])
            else:  # even, horizontal
                walls = self.walls_h[iy // 2, :]
                line = '#'.join(['#' if w else ' ' for w in walls])
                line = '#' + line + "#"
            lines.append(line)

        with open(filename, 'w') as f:
            f.write('\n'.join(lines[::-1]))

    def inner_walls_density(self):
        """Returns the share of inner walls in respect to maximum number of walls when all maze cells are closed.

        Returns
        -------
        share : float
            The density of inner walls.
        """
        return ((np.sum(self.walls_h[1:-1, :]) + np.sum(self.walls_v[:, 1:-1])) /
                (self.walls_h[1:-1, :].size + self.walls_v[:, 1:-1].size))

    def sparsify_maze(self, target_walls_density=1):
        """Sparsifies the maze by randomly reducing the number of walls until the target share is reached. Walls share
        is defined as the number of inner walls divided by total possible number of inner walls (each cell of the maze
        is closed). If share of inner walls of the  maze is already below the target_walls_share, maze remains
        unchanged. This method should only be called after maze is initialized by calling MazeExploration.init().

        Parameters
        ----------
        target_walls_density : float
            Target share of inner walls of the maze (should be between 0 and 1).
        """
        while self.inner_walls_density() > target_walls_density:

            if np.random.uniform() > 0.5:
                iy, ix = np.random.randint(1, np.array(self.walls_h.shape) - 1)
                self.walls_h[iy, ix] = False
            else:
                iy, ix = np.random.randint(1, np.array(self.walls_v.shape) - 1)
                self.walls_v[iy, ix] = False

        if self.verbose:
            # Plot maze layout
            self.plot_maze(f'{self.results_dir}/maze.png')
            print(f'Maze sparsified to target inner walls share {self.inner_walls_density():.2f}')

    def update_cell_visibility(self, iy: int, ix: int):
        """Updates the entire maze cell visibility matrix for given agent position. The cell visibility depends on the
        internal walls of the maze.

        Parameters
        ----------
        iy : int
            Row index of a cell visited by the agent.
        ix : int
            Column index of a cell visited by the agent.
        """
        updated = []

        if not self.walls_h[iy, ix] and iy > 0:  # Down
            self.visible[iy - 1, ix] = True
            updated.append([iy - 1, ix])
        if not self.walls_h[iy + 1, ix] and iy < self.ny - 1:  # Up
            self.visible[iy + 1, ix] = True
            updated.append([iy + 1, ix])
        if not self.walls_v[iy, ix] and ix > 0:  # Left
            self.visible[iy, ix - 1] = True
            updated.append([iy, ix - 1])
        if not self.walls_v[iy, ix + 1] and ix < self.nx - 1:  # Right
            self.visible[iy, ix + 1] = True
            updated.append([iy, ix + 1])
        return updated

    def update_system_matrix(self, local_update=None):
        """The function updates "global" system matrix of shape [ny*nx, ny*nx] that corresponds to finite difference
        approximation of the heat partial differential equation. Initially, when the system is not yet defined, the
        matrix is created for all visible cells. The values in matrix rows corresponding to invisible cells are set to
        zero. The system matrix MazeExploration. A is generally not solvable (at least until all cells are visible),
        but its sub-matrix extracted for visible cells is (rows and columns corresponding to visible cells).

        Parameters
        ----------
        local_update : None or list[int]
            List of two integers representing row and column indices of a cell which surrounding (left/right and
            up/down cells) will be updated in existing system matrix. If local_update is not set (is None) a system
            matrix is formed "from skratch" for all visible cells.
        """
        nxy = self.ny * self.nx

        iy_range = np.arange(self.ny)
        ix_range = np.arange(self.nx)
        if self.A is None:
            self.A = np.zeros([nxy, nxy])
        elif local_update is not None:
            iy_range = np.arange(np.max([local_update[0] - 1, 0]), np.min([local_update[0] + 2, self.ny]))
            ix_range = np.arange(np.max([local_update[1] - 1, 0]), np.min([local_update[1] + 2, self.nx]))

        for iy in iy_range:
            for ix in ix_range:

                # Skip the cell if not visible
                if not self.visible[iy, ix]:
                    continue

                k = iy * self.nx + ix

                if self.solver == 'SOR':
                    if self.visited[iy, ix] <= 1:
                        scale, neigbour = self.SORprep(iy, ix)
                        self.scale[k] = scale
                        for i, v in enumerate(neigbour):
                            self.neigbours[k, i] = v
                        if self.visited[iy, ix]:
                            self.B[k] = 0
                            # Jacobi update
                        self.u[k] = (self.u[self.neigbours[k, 0]] + self.u[self.neigbours[k, 1]] + self.u[
                            self.neigbours[k, 2]] + self.u[self.neigbours[k, 3]] + self.B[k]) / scale

                elif self.solver == 'DGESV':
                    # print(f'{iy=}, {ix=}, {k=}')
                    self.A[k, k] = -4 - self.alpha

                    WallDown = self.walls_h[iy, ix] or not self.visible[iy - 1, ix]
                    WallUp = self.walls_h[iy + 1, ix] or not self.visible[iy + 1, ix]
                    WallLeft = self.walls_v[iy, ix] or not self.visible[iy, ix - 1]
                    WallRight = self.walls_v[iy, ix + 1] or not self.visible[iy, ix + 1]
                    # Down wall
                    if WallDown:
                        self.A[k, k] += 1
                    else:
                        self.A[k, k - self.nx] = 1
                        if self.neumann_bc_order == 2 and WallUp:
                            self.A[k, k - self.nx] = 2
                            self.A[k, k] -= 1
                            # Up wall
                    if WallUp:
                        self.A[k, k] += 1
                    else:
                        self.A[k, k + self.nx] = 1
                        if self.neumann_bc_order == 2 and WallDown:
                            self.A[k, k + self.nx] = 2
                            self.A[k, k] -= 1

                    # Left wall                  
                    if WallLeft:
                        self.A[k, k] += 1
                    else:
                        self.A[k, k - 1] = 1
                        if self.neumann_bc_order == 2 and WallRight:
                            self.A[k, k - 1] = 1
                            self.A[k, k] -= 1
                    # Right wall
                    if WallRight:
                        self.A[k, k] += 1
                    else:
                        self.A[k, k + 1] = 1
                        if self.neumann_bc_order == 2 and WallLeft:
                            self.A[k, k + 1] = 1
                            self.A[k, k] -= 1

    def sor_update_system(self, iy, ix):
        """The function updates list of nodes for the SOR method or system matrix for the Gauss direct method.

        Parameters
        ----------
        iy, ix : Coordinates of points to update
        """


        # Skip the cell if not visible
        if not self.visible[iy, ix]:
            return

        k = iy * self.nx + ix
        if self.visited[iy, ix] <= 1:
            scale, neigbour = self.SORprep(iy, ix)
            self.scale[k] = scale
            for i, v in enumerate(neigbour):
                self.neigbours[k, i] = v
            if self.visited[iy, ix]:
                self.B[k] = 0
                # Jacobi update
            self.u[k] = (self.u[self.neigbours[k, 0]] + self.u[self.neigbours[k, 1]] + self.u[self.neigbours[k, 2]] +
                         self.u[self.neigbours[k, 3]] + self.B[k]) / scale


    def addSOR(self, iy, ix):
        """
        Helper function for SOR method, sets elements in lists
        Parameters
        ----------
        iy : TYPE
            coordinate
        ix : TYPE
            coordinate

        Returns
        -------
        """
        if self.visited[iy, ix]: return
        k = iy * self.nx + ix
        scale, neigbour = self.SORprep(iy, ix)
        red = (iy + ix) % 2
        if red == 0:
            if k in set(self.zR): return
            self.zR = np.append(self.zR,k)
        else:
            if k in set(self.zB): return
            self.zB = np.append(self.zB,k)
        self.scale[k] = scale
        for i, v in enumerate(neigbour):
            self.neigbours[k, i] = v
        self.B[k] = 1
        # Jacobi update
        self.u[k] = (np.sum(self.u[neigbour]) + self.B[k]) / scale

    def SORprep(self, iy, ix):
        """
        Helper function for SOR method 
        Parameters
        ----------
        iy : TYPE
            coordinate
        ix : TYPE
            coordinate

        Returns
        -------
        scale : TYPE
            scale factor of the node
        neigbour : TYPE
            list of neighbouring nodes

        """
        k = iy * self.nx + ix
        scale = 4 + self.alpha
        neigbour = []
        WallDown = self.walls_h[iy, ix] or not self.visible[iy - 1, ix]
        WallUp = self.walls_h[iy + 1, ix] or not self.visible[iy + 1, ix]
        WallLeft = self.walls_v[iy, ix] or not self.visible[iy, ix - 1]
        WallRight = self.walls_v[iy, ix + 1] or not self.visible[iy, ix + 1]
        # Down wall
        if WallDown:
            scale -= 1
        else:
            neigbour.append(k - self.nx)
            if WallUp and self.neumann_bc_order == 2:
                neigbour.append(k - self.nx)
                scale += 1
                # Up wall
        if WallUp:
            scale -= 1
        else:
            neigbour.append(k + self.nx)
            if WallDown and self.neumann_bc_order == 2:
                neigbour.append(k + self.nx)
                scale += 1
        # Left wall
        if WallLeft:
            scale -= 1
        else:
            neigbour.append(k - 1)
            if WallRight and self.neumann_bc_order == 2:
                neigbour.append(k - 1)
                scale += 1
                # Right wall
        if WallRight:
            scale -= 1
        else:
            neigbour.append(k + 1)
            if WallLeft and self.neumann_bc_order == 2:
                neigbour.append(k + 1)
                scale += 1
        return scale, neigbour

    def SORGaussSidelBLOKFor(self, SORomega=1.75, eps=1.e-12, ScaleB=1., maxIteration=10000):
        """
        #SOR succesive over relaxation black and red algorithm
        u-initial/latest solution of LS, 
        maxIteration  permissible number of iteration to obtain a solution with sufficient accuracy
        return information about convergence, number of iteration and new value of LS solution
        Parameters
        ----------
        SORomega : float
            relaxation parameter which must be in [1,2]
        eps : float
            relative error stoping criteria
        ScaleB : float
            scale of the right hand size
            
        maxIteration : int
            safety condition to stop the iterations
            
        Returns
        -------
            convergence : bool
                True if the method converged
            iterations : int
                Total number of iterations
        """

        maxError = np.inf
        iteration = 0

        while maxError > eps and iteration < maxIteration:
            maxError = err0 = err1 = 0
            iteration += 1
            # update red
            if np.size(self.zR)>0:
                du = SORomega * (self.u[self.neigbours[self.zR, 0]] + self.u[self.neigbours[self.zR, 1]] + self.u[
                    self.neigbours[self.zR, 2]] + self.u[self.neigbours[self.zR, 3]] + ScaleB * self.B[self.zR]) / \
                     self.scale[self.zR] - SORomega * self.u[self.zR]
                self.u[self.zR] += du
                # err0=np.max(abs(du))
                err0 = np.max(np.abs(du) / (np.abs(self.u[self.zR]) + 1.e-15))

            # update black
            if np.size(self.zB)>0:
                du = SORomega * (self.u[self.neigbours[self.zB, 0]] + self.u[self.neigbours[self.zB, 1]] + self.u[
                    self.neigbours[self.zB, 2]] + self.u[self.neigbours[self.zB, 3]] + ScaleB * self.B[self.zB]) / \
                     self.scale[self.zB] - SORomega * self.u[self.zB]
                self.u[self.zB] += du
                # err1=np.max(abs(du))
                err1 = np.max(np.abs(du) / (np.abs(self.u[self.zB]) + 1.e-15))

            maxError = max(err0, err1)

        return maxError < eps and iteration < maxIteration, iteration

    def optimal_SOROmega(self):
        """Calculates optimal relaxation parameter for current visible nodes"""
        t0=time.time()
        tmp=np.copy(self.u)   
        def func(omega):
            self.u*=0.
            return self.SORGaussSidelBLOKFor( SORomega=omega, eps=1.e-4,maxIteration=500)[1]
        result=scipy.optimize.fminbound(func, 1., 2., xtol=0.0001)
        if self.verbose:
            print(f'Optimal SOR parmaeter search: {result}, time:{time.time()-t0} s')
        self.u=tmp
        return result
    def enforcePositiveU(self):
        m=np.min(self.u[:-1])
        if(m>=0.): return False
        self.u[self.u<0.]=2.2250738585072014e-308       
        return True

    def calculate_potential(self):
        """Calculates the potential in each of the maze's cells based on the heat equation. The calculation
        acknowledges the visibility of cells and internal walls / topology of the maze.

        Constructing the linear system for calculating potential field. The linear system is constructed for entire
        maze to provide simplicity and readability of the code. Later, only visible cells are filtered out and used
        for solving the potential.
        
        Linear equation for finite difference scheme of the heat partial differential  equation used for regulating
        the potential
            u[i-1,j] - 2*u[i,j] + u[i+1,j] + u[i,j-1] -2*u[i,j] + u[i,j+1] = alpha*u[i,j] - s[i,j]

        Returns
        -------
        computational_time: float
            A computational time consumed for solving the linear system.
        """

        # Solving the linear system
        t1 = time.time()
        iterations = 0
        if self.solver == 'DGESV':
            # Filtering the linear system only for visible cells
            Sf = -1  # Negative for easier/faster right-hand values handling
            # b = np.zeros(nxy)
            v = self.visible.reshape(-1)
            A = self.A[v,][:, v]
            b = (self.visited.reshape(self.ny * self.nx) < 1) * Sf  # .astype(float)
            u = np.linalg.solve(A, b[v])
        elif self.solver == 'SOR':
            status, iterations = self.SORGaussSidelBLOKFor(SORomega=self.SORomega, eps=self.SORomega, ScaleB=self.ScaleB)
            self.totalIterations += iterations
            if not status:
                print(f"convergence error it={iterations}")
                iterations *= -1
        t2 = time.time()

        if self.solver == 'DGESV':
            self.U[self.visible] = u
        elif self.solver == 'SOR':
            iy_range = np.arange(self.ny)
            ix_range = np.arange(self.nx)
            for iy in iy_range:
                for ix in ix_range:
                    k = iy * self.nx + ix
                    self.U[iy, ix] = self.u[k]
        self.U[np.logical_not(self.visible)] = np.nan

        return t2 - t1, iterations

    def explore(self):
        """The main loop of the maze exploration algorithm. At the end of exploration, the execution time figure is
        created. If MazeExploration.plot is True, frames are generated at each time step and eventually compiled in
        an animation video."""

        if self.verbose:
            print(f'Maze shape: ({self.nx}, {self.ny})')
            w_cnt = np.sum(self.walls_h[1:-1, :]) + np.sum(self.walls_v[:, 1:-1])
            print(f'Maze inner walls count: {w_cnt}, ({100 * self.inner_walls_density():.2f}%)')

        t0 = time.time()
        # Update visit counts
        comp_time = np.full([2, self.na], np.nan)
        sor_iterations = np.full(self.na, np.nan)
        for ia in range(self.na):

            t1 = time.time()

            iy, ix = self.agents[ia, self.it, :]

            self.visible[iy, ix] = True
            # add new record in the SOR update list
            # it must be done before it is set to visited

            if self.solver == 'SOR':
                sor_t0 = self.sor_add_new_cell([[iy, ix]])

            self.visited[iy, ix] += 1
            updated = self.update_cell_visibility(iy, ix)
            if self.solver == 'SOR':
                # self.update_system_matrix([iy, ix])
                self.sor_add_new_cell(updated)
                self.sor_update_system(iy, ix)
            if self.solver == 'DGESV':
                self.update_system_matrix([iy, ix])

            comp_time[0, ia] = time.time() - t1
        
        if self.solver == 'SOR':
            if self.it % self.refreshSORomegaStep==0:
                # Corrected omega can reduce computational errors
                self.SORomega = self.optimal_SOROmega()
        # if self.solver == 'DGESV':
        #     self.update_system_matrix()

        comp_time[1, 0], sor_iterations[0] = self.calculate_potential()

        # Update convergence
        self.history['preparation_time'] = np.append(self.history['preparation_time'], [comp_time[0, :]], axis=0)
        self.history['solver_time'] = np.append(self.history['solver_time'], [comp_time[1, :]], axis=0)
        self.history['step_time'] = np.append(self.history['step_time'], time.time() - t0)
        self.history['coverage'] = np.append(self.history['coverage'], np.sum(self.visited > 0) / self.visited.size)
        self.history['sor_iterations'] = np.append(self.history['sor_iterations'], [sor_iterations], axis=0)

        if self.plot:
            self.plot_maze()

        if self.verbose:
            print(f'it={self.it:3d}, {100 * np.sum(self.visited > 0) / self.visited.size:6.2f}% of the maze is explored,'
                  f' solver time: {np.nanmean(comp_time) * 1e3:.3f} ms')

        while True:
            self.it += 1
            self.exploration_step()

            if self.plot and self.it % self.plotstep==0:
                self.plot_maze()

            if self.visited.min() == 1:
                if self.verbose:
                    print('Entire maze is explored', end='')
                break
            if not self.target is None:
                if self.visited[self.target[0], self.target[1]]:
                    if self.verbose:
                        print(f'Target cell {self.target} is found', end='')
                    break
            if self.it >= self.max_it:
                if self.verbose:
                    print('Maximum number of iterations is reached', end='')
                break

        # Total time
        self.history['total_time'] = time.time() - t0

        if self.verbose:
            print(f' in {self.it} steps, solver time: {np.nansum(self.history["solver_time"]):.3f} s, total time: {self.history["total_time"]:.3f} s')
            print(f'Average steps needed to find the target cell: {np.mean(self.first_visit):.1f}')

            self.plot_maze(f'{self.results_dir}/final_state.png')

        # Make computational times figure
        if self.verbose:
            fig, [ax1, ax2, ax3] = plt.subplots(figsize=(6, 4.5), nrows=3, sharex='all',
                                                tight_layout=True, dpi=200, height_ratios=(2.5, 1, 1))
            t_prep = np.nanmean(self.history['preparation_time'], axis=1)
            t_calc = np.nanmean(self.history['solver_time'], axis=1)
            t_other = self.history["step_time"] - t_prep - t_calc
            sor_iterations = np.nanmean(self.history['sor_iterations'], axis=1)
            # print(t_prep)
            # print(t_calc)

            t_total = t_calc + t_prep
            times = np.arange(self.it + 1)
            pol=np.polyfit(times, t_total, 1)
            p1 = np.poly1d(pol)
            # print(pol)
            ax1.bar(np.arange(self.it + 1), t_prep * 1e3,
                    width=1, alpha = 0.5,
                    label=f'Preparation (Total CPU time: {np.sum(t_prep):0.3f} s)')
            ax1.bar(np.arange(self.it + 1), t_calc * 1e3, bottom=t_prep * 1e3,
                    width=1, alpha=0.5,
                    label=f'Linear system solver (Total CPU time: {np.sum(t_calc):0.3f} s)')
            ax1.bar(np.arange(self.it + 1), t_other * 1e3, bottom=t_total * 1e3,
                    width=1, color='grey', alpha=0.5,
                    label=f'Other time (Total CPU time: {np.sum(t_other - t_total):0.3f} s)')
            # ax1.plot(np.arange(self.it + 1), t_total * 1e3, 'r', label='t_total')
            # ax1.set_title('Computational time')
            ax1.set_yscale('log')
            # ax1.set_ylim(1e-4, np.nanmax(t_prep + t_calc) * 1e3)
            #ax1.set_ylim(0, 0.08)
            # ax1.set_xlabel('Steps')
            ax1.set_ylabel('CPU time [ms]')
            ax1.set_xlim(0, self.it)
            ax1.legend(frameon=False)


            # ax2.plot(np.arange(self.it + 1), 100 * self.history['coverage'])
            ax2.bar(np.arange(self.it + 1), 100 * self.history['coverage'],
                    width=1, color='darkgreen', alpha=0.5,)
            ax2.set_ylim(0, 100)
            # ax2.set_title('Coverage')
            # ax2.set_xlabel('Steps')
            ax2.set_ylabel('Visited\ncells [%]')

            ax3.bar(np.arange(self.it + 1), sor_iterations, label='SOR iterations', width=1)
            ax3.set_ylim(0, np.nanmax(sor_iterations) + 1)
            # ax3.set_yscale('log')
            # ax3.set_title('SOR solver performance')
            ax3.set_xlabel('Exploration steps')
            ax3.set_ylabel('SOR\niterations')

            for ax in [ax1, ax2, ax3]:
                ax.grid(ls=':', color='grey', lw=0.5)

            fig.align_ylabels()
            fig.savefig(f'{self.results_dir}/convergence.png')
            plt.close(fig)
            # plt.show()

        np.savez_compressed(f'{self.results_dir}/results.npz',
                            init_positions=self.agents[:, 0, :],
                            first_visit=self.first_visit,
                            steps=self.it,
                            **self.history)

        # Make the animation using ffmpeg
        if self.plot:  # Only if frames are already made
            cmd = 'ffmpeg -r 2 -i frame_%05d.png -c:v libx264 -vf "fps=25,pad=ceil(iw/2)*2:ceil(ih/2)*2" -y -pix_fmt yuv420p maze_exploration.mp4'.split()
            # cmd = f'c:\ffmpeg\bin\ffmpeg.exe -r 2 -i frame_%05d.png -c:v libx264 -vf "fps=25,pad=ceil(iw/2)*2:ceil(ih/2)*2" -y -pix_fmt yuv420p maze_exploration.mp4'.split()
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.results_dir)
            output, error = p.communicate()
            print(output.decode())
            print(error)

    def exploration_step(self):
        """A single time step of the maze exploration in which all agent make a single cell move according to the
        potential at maze's cells. For each agent step, visibility of cells and system matrix is updated according to
        new agent position, and potential is calculated accordingly."""

        t0 = time.time()
        comp_time = np.full([2, self.na], np.nan)
        sor_iterations = np.full(self.na, np.nan)

        # Move agents
        for ia in range(self.na):

            # Calculate the potential for visible cells of the maze
            comp_time[1, ia], sor_iterations[ia] = self.calculate_potential()

            t1 = time.time()
            iy, ix = self.agents[ia, self.it - 1, :]
            # Obtain all possible moves for an agent (considering maze walls and possible collisions)
            possible_moves = np.full([0, 2], 0, dtype=int)
            u = np.array([])
            if not self.walls_h[iy, ix] and (self.allow_collision or not self.occupied[iy - 1, ix]):  # Down
                possible_moves = np.append(possible_moves, np.array([[iy - 1, ix]]), axis=0)
                u = np.append(u, self.U[iy - 1, ix])
            if not self.walls_h[iy + 1, ix] and (self.allow_collision or not self.occupied[iy + 1, ix]):  # Up
                possible_moves = np.append(possible_moves, np.array([[iy + 1, ix]]), axis=0)
                u = np.append(u, self.U[iy + 1, ix])
            if not self.walls_v[iy, ix] and (self.allow_collision or not self.occupied[iy, ix - 1]):  # Left
                possible_moves = np.append(possible_moves, np.array([[iy, ix - 1]]), axis=0)
                u = np.append(u, self.U[iy, ix - 1])
            if not self.walls_v[iy, ix + 1] and (self.allow_collision or not self.occupied[iy, ix + 1]):  # Right
                possible_moves = np.append(possible_moves, np.array([[iy, ix + 1]]), axis=0)
                u = np.append(u, self.U[iy, ix + 1])

            # Move the agent and update visibility
            if u.size > 0:
                # Set cell occupancy for old position
                self.occupied[iy, ix] = False
                # Move if there is at least one possible move/cell
                iy, ix = possible_moves[np.argmax(u), :]
            self.agents[ia, self.it, :] = iy, ix
            updated = self.update_cell_visibility(iy, ix)
            if self.visited[iy, ix] == 0:
                self.first_visit[iy, ix] = self.it
            # Update visit counts
            self.visited[iy, ix] += 1
            # self.update_system_matrix([iy, ix])

            if self.solver == 'SOR':
                self.sor_add_new_cell(updated)
                self.sor_update_system(iy, ix)
            else:
                self.update_system_matrix([iy, ix])

            # Set cell occupancy for new position
            self.occupied[iy, ix] = True

            comp_time[0, ia] = time.time() - t1

        # Update convergence
        self.history['preparation_time'] = np.append(self.history['preparation_time'], [comp_time[0, :]], axis=0)
        self.history['solver_time'] = np.append(self.history['solver_time'], [comp_time[1, :]], axis=0)
        self.history['step_time'] = np.append(self.history['step_time'], time.time() - t0)
        self.history['coverage'] = np.append(self.history['coverage'], np.sum(self.visited > 0) / self.visited.size)
        self.history['sor_iterations'] = np.append(self.history['sor_iterations'], [sor_iterations], axis=0)

        # Solve the potential at the end of step/iteration (for visualization)
        if self.plot:
            self.calculate_potential()

        if self.solver == 'SOR':
            if self.it % self.refreshSORomegaStep==0:
                #corrected omega can reduce computational errors
                self.SORomega=self.optimal_SOROmega()
            
        if self.verbose:
            print(f'it={self.it:3d}, {100 * np.sum(self.visited > 0) / self.visited.size:6.2f}% of the maze is explored,'
              f' {np.nanmean(comp_time) * 1e3:.3f} ms elapsed')

    def plot_maze(self, filename=None):
        """Visualization of the maze exploration at the current time step. Visualization contains maze layout, current
        agent locations, accomplished agent paths, cell visibility and values of the potential in visible cells.

        Parameters
        ----------
        filename : None or str
            A path to a file to which the maze visualization is saved.
        """

        fig, ax = plt.subplots(figsize=(10, 10 / self.nx * self.ny + 0.1), dpi=200, tight_layout=True)
        ax.axis('off')

        lw = 100 / self.nx
        ms = 300 / self.nx
        ax.imshow(self.visible, zorder=-2, cmap='Greys_r', vmin=-5, vmax=1)
        #if self.U.min() > 0:
        ax.imshow(np.log(self.U), zorder=-1, cmap='summer', alpha=0.4)

        for iy in range(self.ny + 1):
            for ix in range(self.nx):
                if self.walls_h[iy, ix]:
                    ax.plot([ix - 0.5, ix + 0.5], [iy - 0.5, iy - 0.5], 'k-', lw=lw, zorder=2)
        for iy in range(self.ny):
            for ix in range(self.nx + 1):
                if self.walls_v[iy, ix]:
                    ax.plot([ix - 0.5, ix - 0.5], [iy - 0.5, iy + 0.5], 'k-', lw=lw, zorder=2)

        if not self.target is None:
            ax.plot(self.target[1], self.target[0], 'xk', ms=ms, mew=5)

        if self.agents is not None:
            for ia in range(self.na):
                # print(self.agents[ia, :self.it + 1, :])
                iy = self.agents[ia, :self.it + 1, 0]
                ix = self.agents[ia, :self.it + 1, 1]
                for i in range(iy.size - 1):
                    ax.plot(ix[i:i + 2], iy[i:i + 2], '-', c=f'C{ia}', lw=2*lw, alpha=0.3, zorder=2)
                ax.plot(ix[-1], iy[-1], 'o', c=f'C{ia}', ms=ms, zorder=3)
        
        ax.set_title(f't={self.it}, {100 * np.sum(self.visited > 0) / self.visited.size:6.2f}% explored')
        ax.axis('equal')
        ax.set_xlim(-0.55, self.nx - 0.45)
        ax.set_ylim(-0.55, self.ny + 0.45)
        if filename:
            plt.savefig(filename, dpi=100)
        else:
            plt.savefig(f'{self.results_dir}/frame_{self.it:05d}.png', dpi=100)
        plt.close(fig)


if __name__ == '__main__':
    
    maze = MazeExploration(results_dir='results_50x50', maze='layouts/maze_50x50.txt')
    # maze = MazeExploration(results_dir='results_50x50_sparse', maze='layouts/maze_50x50_sparse.txt')
    maze.solver = 'SOR'
    # maze.solver = 'DGESV'
    maze.neumann_bc_order = 2
    maze.alpha = 2.0
    maze.SORomega = 1.1
    maze.eps = 1e-4
    maze.maxIteration = 1000
    maze.ScaleB = 1e50
    
    maze.plot = False
    maze.plotstep=10
    # maze.visible = True # Entire maze layout is known (default is False - maze layout is unknown)
    maze.allow_collision = True

    """
    Initial agent locations
    MazeExploration.agents parameter is a nx2 integer matrix with initial locations 
    of agents (x and y indices of maze cell). If agents parameter is se as single integer,
    it indicates number of agents while their initial locations are randomly determined.
    """
    # Manually place two agents
    # maze.agents = np.array([[49, 49],
    #                         [0, 0]])
    
    # Manually place six agents
    # maze.agents = np.array([[9, 9],
    #                         [0, 0],
    #                         [30, 30],
    #                         [20, 20],
    #                         [39, 39],
    #                         [49, 49]])
    
    # Randomly place 3 agents
    maze.agents = 3 

    maze.init()
    maze.sparsify_maze(0.4) # Randomly removes inner walls
    
    maze.explore() # Start the exploration
    
