# -*- coding: utf-8 -*-
"""
This is a utility script that generates layouts for mazes of different shapes, and
randomized configurations for agent initial locations. These layouts and 
agent configurations are used in validation procedures presented in the paper:

"Fast algorithm for centralized multi-agent maze exploration." by
Crnković, Bojan, Stefan Ivić, and Mila Zovko
arXiv preprint arXiv:2310.02121 (2023)
https://arxiv.org/abs/2310.02121
"""

import os.path
import numpy as np
from mazelib import Maze
from mazelib.generate.BacktrackingGenerator import BacktrackingGenerator
from maze_exploration import MazeExploration

configuration_dir = 'mazes_exploration_configurations/'

#                       shape,      density,    layouts,    init_confs,     agents
maze_configurations = [[(10, 10),   0.45,       20,         5,               (1, 2, 3, 4, 5)],  # 0
                       [(10, 10),   0.3,        20,         5,               (1, 2, 3, 4, 5)],  # 1
                       [(20, 20),   0.475,      20,         5,               (1, 3, 5, 10)],  # 2
                       [(20, 20),   0.3,        20,         5,               (1, 3, 5, 10)],  # 3
                       [(50, 50),   0.4,        10,         5,               (5, 10, 20)],  # 4
                       [(50, 50),   0.3,        10,         5,               (5, 10, 20)],  # 5
                       [(100, 100), 0.4,        10,         5,               (10, 20)],  # 6
                       [(100, 100), 0.3,        10,         5,               (10, 20)],  # 7
                       [(150, 200), 0.3,        10,         5,               (20, )],  # 8
                       [(150, 400), 0.3,        10,         5,               (20, )],  # 9
                       ]

if __name__ == '__main__':

    agent_configurations = list(set([(maze[0], maze[3], maze[4]) for maze in maze_configurations]))
    for (w, h), nc, na in agent_configurations:
        print(w, h, nc, na)
        conf_dir = f'{configuration_dir}/agent_configs_{w}x{h}'
        if not os.path.exists(conf_dir):
            os.makedirs(conf_dir)

        for a in na:
            for c in range(nc):
                conf_file = f'{conf_dir}/na{a}_config{c:03d}.txt'
                ixy = []
                ia = 0
                while ia < a:
                    ix, iy = np.random.randint([h, w])
                    if (ix, iy) not in ixy:
                        ixy.append((ix, iy))
                        ia += 1
                with open(conf_file, 'w') as f:
                    for ix, iy in ixy:
                        f.write(f'{ix} {iy}\n')


    for maze in maze_configurations:

        (w, h), density, samples, _, _ = maze

        shape_dir = f'{configuration_dir}/mazes_{w}x{h}_w{density * 100:.2f}'
        if not os.path.exists(shape_dir):
            os.makedirs(shape_dir)

        for sample in range(samples):

            maze_file = f'{shape_dir}/maze_{sample:03d}.txt'
            print(f'{maze_file}, ', end='')

            if os.path.exists(maze_file):
                print('exists, skipping')
                continue

            print('generating maze')
            m = Maze()
            m.generator = BacktrackingGenerator(h, w)
            m.generate()
            with open(maze_file, 'w') as f:
                f.write(m.tostring())

            maze = MazeExploration(f'/tmp/maze', maze_file)
            maze.verbose = False
            maze.init()
            maze.sparsify_maze(density)
            maze.save_ascii_maze(maze_file)
            maze.plot_maze(f'{maze_file[:-4]}.png')


