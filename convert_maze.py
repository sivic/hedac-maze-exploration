# -*- coding: utf-8 -*-
"""
This is a utility script for converting existing layouts stored in alternative 
file formats to the format suitable for MazeExploration class.

"Fast algorithm for centralized multi-agent maze exploration." by
Crnković, Bojan, Stefan Ivić, and Mila Zovko
arXiv preprint arXiv:2310.02121 (2023)
https://arxiv.org/abs/2310.02121
"""

import numpy as np

"""
A simple converter for maze files avaliable at https://github.com/micromouseonline/mazefiles/tree/master
"""
def convert_maze(filename, new_name):
    with open(filename) as maze_file:
        lines = maze_file.readlines()

    hwalls = []
    for line in lines[0::2]:
        hwalls.append([c == '---' for c in line.split('o')[1:-1]])
    hwalls = np.array(hwalls, dtype=int)
    hwalls = hwalls[1:-1, :]
    print(f'{hwalls.shape=}')
    print(hwalls)

    vwalls = []
    for line in lines[1::2]:
        vwalls.append([c == '|' for c in line[0::4]])
    vwalls = np.array(vwalls, dtype=int)
    vwalls = vwalls[:, 1:-1]
    print(f'{vwalls.shape=}')
    print(vwalls)

    np.savetxt(f'{new_name}_h.txt', hwalls)
    np.savetxt(f'{new_name}_v.txt', vwalls)

"""
A simple converter for maze files generated at https://www.dcode.fr/maze-generator
"""
def convert_maze_ascii(filename, new_name):
    with open(filename) as maze_file:
        lines = maze_file.readlines()
    print(len(lines))
    hwalls = []
    for line in lines[0::2]:
        hwalls.append([c == '#' for c in line.strip()[1::2]])
    hwalls = np.array(hwalls, dtype=int)
    hwalls = hwalls[1:-1, :]
    print(f'{hwalls.shape=}')
    #print(hwalls)

    vwalls = []
    for line in lines[1::2]:
        vwalls.append([c == '#' for c in line.strip()[0::2]])
    vwalls = np.array(vwalls, dtype=int)
    vwalls = vwalls[:, 1:-1]
    print(f'{vwalls.shape=}')
    #print(vwalls)

    np.savetxt(f'{new_name}_h.txt', hwalls, fmt='%d')
    np.savetxt(f'{new_name}_v.txt', vwalls, fmt='%d')

if __name__ == '__main__':
    convert_maze_ascii('maze_200x200.txt', 'maze_200x200')