# -*- coding: utf-8 -*-
"""
This is an example script for the maze exploration algorithm presented in

"Fast algorithm for centralized multi-agent maze exploration." by
Crnković, Bojan, Stefan Ivić, and Mila Zovko
arXiv preprint arXiv:2310.02121 (2023)
https://arxiv.org/abs/2310.02121
"""
import numpy as np
from mazelib import Maze
from mazelib.generate.Prims import Prims
from mazelib.generate.Ellers import Ellers
from mazelib.generate.CellularAutomaton import CellularAutomaton
from mazelib.generate.BacktrackingGenerator import BacktrackingGenerator
import convert_maze
from maze_exploration import MazeExploration
from prepare_maze_configurations import configuration_dir

"""
Generate maze layout (try other mazelib classes)
"""
w, h = 50, 50
m = Maze()
m.generator = BacktrackingGenerator(h, w)
m.generate()
# print(m.tostring())

with open(f'layouts/maze_{w}x{h}.txt', 'w') as f:
    f.write(m.tostring())

"""
Convert existing maze layouts
- use convert_maze for mazes from https://www.dcode.fr/maze-generator
- use convert_maze_ascii for mazes from https://github.com/micromouseonline/mazefiles/tree/master
"""
# convert_maze.convert_maze('maze_example.txt', 'maze_example')
# convert_maze.convert_maze_ascii('maze_example.tmp', f'maze_example')

# Use above generated maze layout and 5 randomly placed agents
maze = MazeExploration(f'maze_{w}x{h}.txt', f'layouts/maze_{w}x{h}.txt')
maze.agents = 5

# Use pregenerated layouts and agent configurations
# maze = MazeExploration(f'test', f'{configuration_dir}/mazes_150x400_w30.00/maze_000.txt') 
# maze.agents = np.loadtxt(f'{configuration_dir}/agent_configs_150x400/na20_config000.txt', ndmin=2)

maze.solver = 'SOR' # 'DGESV' or 'SOR'
maze.alpha = 0.3
maze.eps = 1e-4
maze.SORomega = 1.4
maze.neumann_bc_order = 2
maze.refreshSORomegaStep = 100
maze.plot = False

maze.init()
# maze.sparsify_maze(0.3) # Keep at most 30% of all possible walls (note that maze can have walls as much as a perfect maze of same shape have)
# maze.save_ascii_maze('layouts/sparse_maze.txt') # Saving a sparsified maze layout
# maze.plot_maze('sparse_maze.png')
maze.explore()

