# -*- coding: utf-8 -*-
"""
This is a script for runnging all layouts and agent configurations defined in
maze_configuration.py script.

"Fast algorithm for centralized multi-agent maze exploration." by
Crnković, Bojan, Stefan Ivić, and Mila Zovko
arXiv preprint arXiv:2310.02121 (2023)
https://arxiv.org/abs/2310.02121
"""

import os
import shutil
import numpy as np
from joblib import Parallel, delayed
from prepare_maze_configurations import configuration_dir, maze_configurations
from maze_exploration import MazeExploration


results_dir = 'hedac_maze_exploration/mazes_exploration_results/'

#                            solver,    AC,     known maze, maze_config
algorithm_configurations = [('DGESV',   True,   False, [0, 1, 2, 3, 4, 5]),
                            ('DGESV',   False,  False, [0, 1, 2, 3, 4, 5]),
                            ('SOR',     True,   False, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]),
                            ('SOR',     False,  False, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]),
                            ('SOR',     True,   True, [0, 1, 2, 3, 4, 5]),
                            ('SOR',     False,  True, [0, 1, 2, 3, 4, 5]),
                            ('Alian',   True,   False, [0, 1, 2, 3, 4, 5]),
                            ('KC',      False,  False, [0, 1, 2, 3, 4, 5]),
                            ]


def single_run(maze_conf_dir, solver, anti_collision, known_maze, na, layout, aconf):
    # maze_conf_dir, solver, anti_collision, known_maze, na, layout, aconf = args
    lbl = (f'{solver} AC{"on" if anti_collision else "off"} '
           f'{"known" if known_maze else "unknown"} '
           f'maze_{w}x{h}, w={density}, na={na}, layout={layout}, aconf={aconf}')
    res_file = (f'{maze_conf_dir}/{solver}_AC{"on" if anti_collision else "off"}_'
                f'{"known" if known_maze else "unknown"}_{na=}_{layout=:03d}_{aconf=:03d}.npz')
    if os.path.exists(res_file):
        print(f'{lbl}, already exist, skipping')
        return

    shape_dir = f'{configuration_dir}/mazes_{w}x{h}_w{density * 100:.2f}'
    maze_file = f'{shape_dir}/maze_{layout:03d}.txt'

    conf_dir = f'{configuration_dir}/agent_configs_{w}x{h}'
    conf_file = f'{conf_dir}/na{na}_config{aconf:03d}.txt'

    # maze = MazeExploration(f'{results_dir}/tmp', maze_file)
    maze = MazeExploration(res_file[:-4], maze_file)
    maze.solver = solver
    maze.alpha = 0.3
    maze.eps = 1e-4
    maze.SORomega = 1.4
    maze.neumann_bc_order = 2
    maze.refreshSORomegaStep = 100

    maze.plot = False
    # maze.plotstep = 5
    maze.visible = known_maze  # Entire maze is known
    maze.allow_collision = not anti_collision

    maze.verbose = False  # Suppress the output
    maze.agents = np.loadtxt(conf_file, ndmin=2)
    maze.init()
    maze.explore()

    os.rename(f'{res_file[:-4]}/results.npz', res_file)
    shutil.rmtree(res_file[:-4])
    print(f'{lbl}, solver time: {np.nansum(maze.history["solver_time"]):.2f} s, total time: {maze.history["total_time"]:.2f} s')


if __name__ == '__main__':

    for solver, anti_collision, known_maze, maze_configs in algorithm_configurations:
        for i_maze_config in maze_configs:
            (w, h), density, layouts, nc, na_list = maze_configurations[i_maze_config]

            maze_conf_dir = f'{results_dir}/mazes_{w}x{h}_w={density}'
            if not os.path.exists(maze_conf_dir):
                os.makedirs(maze_conf_dir)

            for na in na_list:
                batch_arguments = []
                for layout in range(layouts):
                    for aconf in range(nc):
                        agruments = maze_conf_dir, solver, anti_collision, known_maze, na, layout, aconf
                        batch_arguments.append(agruments)

                print()
                print('-' * 100)
                print(f'{solver} AC{"on" if anti_collision else "off"} '
                      f'{"known" if known_maze else "unknown"} '
                      f'maze_{w}x{h}, w={density}, na={na}')
                print('-' * 100)
                Parallel(n_jobs=32)(delayed(single_run)(*args) for args in batch_arguments)
