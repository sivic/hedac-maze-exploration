# hedac-maze-exploration



This is the code Heat Equation Driven Area Coverage (HEDAC) for mutli-agent maze exploration.

This code is a supplementary material for article 

***Fast algorithm for centralized multi-agent maze exploration***

by Bojan Crnković, Stefan ivić and Mila Zovko

## Dependencies

- Numpy
- Matplotlib
- Scipy
- Mazelib
- Joblib

## Simple run of maze exploration

The `__main__` part of  `maze_exploration.py` and `example.py` allow setting up and running a simple single simulation of maze exploration including:

- loading or converting an existing maze layout, and sparsifiying loaded layout
- randomly generating or specifying agents initial configuration
- allowing algorithm to explore while knowing or not knowing entire maze layout (`visible` option)
- trying different linear system solvers (`solver` option)
- setting many other options (HEDAC `alpha` parameter, visualization, tolerances, order of BC, etc.)

## Generating maze layouts and agent configurations

Run `python prepare_maze_configurations.py` in order to generate maze layouts and agent configurations used in maze exploration simulations.

Since above procedure randomly generates maze layouts and agent configurations, we also provide `mazes_exploration_configurations.zip` that contains scenarios that were simulated to exactly produce the results presented in the paper.  

## Running all scenarios

In order to run all generated scenarios (combinations of maze layouts and agent configurations), one can start script `run_all.py`. Please note that, depending on your hardware, these calculations may take several hours or even days to finish.

